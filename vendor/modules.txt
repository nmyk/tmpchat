# github.com/google/uuid v1.3.0
## explicit
github.com/google/uuid
# github.com/gorilla/websocket v1.5.0
## explicit; go 1.12
github.com/gorilla/websocket
# github.com/ianschenck/envflag v0.0.0-20140720210342-9111d830d133
## explicit
github.com/ianschenck/envflag
