package main

import (
	"encoding/json"
	"log"
	"net/http"
	"net/url"
	"tmpchat"

	"github.com/gorilla/websocket"
	"github.com/ianschenck/envflag"
)

var chat = &tmpchat.Chat{
	Channels: make(map[string]*tmpchat.Channel),
}

type signalingHandler struct {
	frontendURL    string
	turnAuthSecret string
}

func (h signalingHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	params := r.URL.Query()
	var channelName, userID string
	if vals, ok := params["channelName"]; ok {
		channelName = vals[0]
	}
	if vals, ok := params["userID"]; ok {
		userID = vals[0]
	}
	var origin string
	upgrader := websocket.Upgrader{
		CheckOrigin: func(*http.Request) bool {

			if vals, ok := r.Header["Origin"]; ok {
				origin = vals[0]
			}
			originURL, err := url.Parse(origin)
			if err != nil {
				return false
			}
			return originURL.String() == h.frontendURL
		},
	}
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print("upgrade: ", err, " ", origin)
		return
	}
	chat.Materialize(channelName, h.turnAuthSecret).Members.Set(userID, conn)
	defer chat.Collect(channelName, userID)
	for {
		_, rawSignal, err := conn.ReadMessage()
		if err != nil {
			log.Println("read:", err)
			break
		}
		message := tmpchat.Message{}
		if err := json.Unmarshal(rawSignal, &message); err != nil {
			continue
		}
		if ch, ok := chat.Get(channelName); ok {
			ch.Messages <- message
		}
	}
}

func main() {
	var (
		frontendURL    string
		turnAuthSecret string
	)
	envflag.StringVar(&frontendURL, "TMPCHAT_URL", "http://frontend:8080", "")
	envflag.StringVar(&turnAuthSecret, "TURN_AUTH_SECRET", "", "")
	envflag.Parse()
	http.Handle("/", signalingHandler{frontendURL, turnAuthSecret})
	log.Fatal(http.ListenAndServe(":5050", nil))
}
