package main

import (
	"embed"
	"html/template"
	"log"
	"net/http"

	_ "embed"

	"github.com/google/uuid"
	"github.com/ianschenck/envflag"
)

//go:embed templates
var content embed.FS

func getTemplate(raw string, funcMap template.FuncMap) *template.Template {
	return template.Must(template.New("").Funcs(funcMap).Parse(raw))
}

type tmpchatHandler struct {
	signalingURL string
}

func (h tmpchatHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	channelName := r.URL.Path[1:] // Omit leading slash in path
	var tmpl *template.Template
	fm := template.FuncMap{
		"safeURL": func(u string) template.URL { return template.URL(u) },
		"safeCSS": func(s string) template.CSS { return template.CSS(s) },
	}
	var pg []byte
	if channelName == "" {
		pg, _ = content.ReadFile("templates/tmpchat-index.gohtml")
	} else {
		pg, _ = content.ReadFile("templates/tmpchat-channel.gohtml")
	}
	style, _ := content.ReadFile("templates/style.css")
	tmpl = getTemplate(string(pg), fm)
	userID := uuid.New().String()
	d := tmpchatPageData{
		string(style),
		channelName,
		userID,
		h.signalingURL,
	}
	_ = tmpl.Execute(w, d)
}

type tmpchatPageData struct {
	Style        string
	ChannelName  string
	UserID       string
	SignalingURL string
}

func main() {
	var signalingURL string
	envflag.StringVar(&signalingURL, "TMPCHAT_SIGNALING_URL", "ws://signaling:5050", "URL for tmpchat signaling server")
	envflag.Parse()
	http.Handle("/", tmpchatHandler{signalingURL})
	log.Fatal(http.ListenAndServe(":8080", nil))
}
