module tmpchat

go 1.19

require (
	github.com/google/uuid v1.3.0
	github.com/gorilla/websocket v1.5.0
	github.com/ianschenck/envflag v0.0.0-20140720210342-9111d830d133
)
